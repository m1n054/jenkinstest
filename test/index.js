let chai = require('chai');
let expect = chai.expect;

describe('Simple test', function(){
	it('should pass', function(){
		expect(1).to.equal(1);
	})
	it('should also pass', function(){
		expect(100).to.equal(100);
	});
});
